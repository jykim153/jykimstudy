from bs4 import BeautifulSoup
import selenium.webdriver as webdriver

def run_driver():
    driver = webdriver.Chrome('/home/jykim/.local/lib/chromedriver.exe')
    driver.implicitly_wait(3)
    return driver

def  instar_search(driver,instar_url):
    driver.get(instar_url)
    return driver

def instar_info(driver,result):
    #div_elem = driver.find_element_by_class_name("Nm9Fw").text
    result = driver.find_element_by_css_selector("#react-root > section > main > div > div > article > div.eo2As > section.EDfFK.ygqzn > div > div > button > span").text
    return result

if __name__ == '__main__':
    result = ""
    instar_url = "https://www.instagram.com/p/B7A5HFLgccT/"
    driver = run_driver()
    driver = instar_search(driver,instar_url)
    result = instar_info(driver,result)
    print("좋아요:", result)
