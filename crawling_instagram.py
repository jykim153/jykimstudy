from selenium import webdriver
from bs4 import BeautifulSoup
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
import requests
import time
from pathlib import Path
from os import getenv


def run_driver():
    chromedriver_path = (getenv("HOME"))+"/Downloads/chromedriver"
    driver = webdriver.Chrome(chromedriver_path)
    driver.implicitly_wait(3)
    return driver
    
def search_from_instagram(driver,search_insta_url,result):
    driver.get(search_insta_url)
    return driver


def get_instaInfo(driver,result):
    
    result['like_num'] = driver.find_element_by_css_selector('#react-root > section > main > div > div > article > div.eo2As > section.EDfFK.ygqzn > div > div > button > span').text    
    result['description'] = driver.find_element_by_css_selector('#react-root > section > main > div > div > article > div.eo2As > div.EtaWk > ul > div > li:nth-child(1) > div > div > div > span').text
    result['owner'] = driver.find_element_by_css_selector('#react-root > section > main > div > div > article > header > div.o-MQd > div.PQo_0 > div.e1e1d > h2 > a').text
    
    ## 댓글 수와 댓글 리스트 가져오는 메소드 추가 개발 완료 -- 인스타 댓글 > 스크롤에서 버튼누른는 방식 업그레이드.... 
    comments_arr = get_comments(driver,result)

    result['comments'] = comments_arr
    result['comments_num'] = len(comments_arr)
    return result

def get_comments(driver,result):
    result_arr = []
    comments_list = scroll_down(driver,result)
    
    for comment in comments_list:
        result_arr.append(comment.find_element_by_css_selector(' div > div > div > span').text)
    return result_arr

def scroll_down(driver,result):
    SCROLL_PAUSE_TIME = 3
    # define initial page height for 'while' loop
    #last_height = driver.execute_script("return document.getElementsByClassName('k59kT')[0].scrollHeight")
    
    while True:        
        time.sleep(SCROLL_PAUSE_TIME)
        is_more_comments = driver.find_elements_by_css_selector('#react-root > section > main > div > div > article > div.eo2As > div.EtaWk > ul > li > div > button > span')
        if is_more_comments :
            driver.find_elements_by_css_selector('#react-root > section > main > div > div > article > div.eo2As > div.EtaWk > ul > li > div > button')[0].click()
        else:
            return driver.find_elements_by_css_selector('#react-root > section > main > div > div > article > div.eo2As > div.EtaWk > ul > ul > div > li')    

        '''
        이전 버전 (댓글 수집 > 스크롤)    
        if len(driver.find_elements_by_css_selector('#react-root > section > main > div > div > article > div.eo2As > div.KlCQn.EtaWk > ul > li.lnrre > button')) > 0 :                    
            driver.find_elements_by_css_selector('#react-root > section > main > div > div > article > div.eo2As > div.KlCQn.EtaWk > ul > li.lnrre > button')[0].click()
        new_height = driver.execute_script("return document.getElementsByClassName('k59kT')[0].scrollHeight")        
        if new_height == last_height:
            return driver.find_elements_by_css_selector('#react-root > section > main > div > div > article > div.eo2As > div.KlCQn.EtaWk > ul > li')    
        else:
            last_height = new_height
        ''' 
    

if __name__ == '__main__':
    result = {}    
    search_insta_url = "https://www.instagram.com/p/Bqkinu_nliN/"
    driver = run_driver()
    driver = search_from_instagram(driver,search_insta_url,result)
    result = get_instaInfo(driver, result)    
    print(result)
    
